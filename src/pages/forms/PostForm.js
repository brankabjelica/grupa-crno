import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import * as yup from 'yup';
import { useFormik } from 'formik';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';

import { LocalizationContext } from '../../context/LanguageContext'

import client from '../../apis/client';
import { Typography } from '@mui/material';


const PostForm = (props) => {
    const { t } = React.useContext(LocalizationContext);
    let navigate = useNavigate()
    const [buttonState, setButtonState] = useState(false)
    const [postData, setPostData] = useState(props.postData || undefined)


    const initialValues = {
        title: postData !== undefined ? postData.title : '',
        text: postData !== undefined ? postData.text : ''
    }


    const postPost = async (values) => {
        try {
            setButtonState(true)

            let post = {
                title: values.title,
                text: values.text,
                profile: props.profileId
            }

            if (postData !== undefined) {

                await client.put(`/profiles/${props.profileId}/posts/${props.postData.id}/`, post)

                alert('Uspesno ste editovali post')

                navigate('/post')
                navigate(0)

             }
            else {

                await client.post(`/profiles/${props.profileId}/posts/`, post)

                alert('Uspesno ste kreirali post')

                navigate('/post')
                navigate(0)
      
            }



        } catch (err) {

            if (err.response) {
                alert(JSON.stringify(err.response.data));
                setButtonState(false)
            }
        }
    };


    const validationSchema = yup.object({
        title: yup.string()
            .required('Obavezno polje'),
        text: yup.string()
            .required('Obavezno polje')
    });


    const formik = useFormik({
        initialValues: initialValues,
        validationSchema: validationSchema,
        onSubmit: postPost,
    });


    return (
        <Container component='main'>
            <Typography m={3} variant='h4'>
                {postData !== undefined ? 'Izmeni post' : 'Kreiraj post'}
            </Typography>
            <form onSubmit={formik.handleSubmit} className='mt-3 m-2'>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id='title'
                            name='title'
                            variant='outlined'
                            label={'Naslov'}
                            value={formik.values.title}
                            onChange={formik.handleChange}
                            error={formik.touched.title && Boolean(formik.errors.title)}
                            helperText={formik.touched.title && formik.errors.title}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id='text'
                            name='text'
                            variant='outlined'
                            label={'Tekst'}
                            multiline={true}
                            rows={4}
                            value={formik.values.text}
                            onChange={formik.handleChange}
                            error={formik.touched.text && Boolean(formik.errors.text)}
                            helperText={formik.touched.text && formik.errors.text}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Button color='primary' variant='contained' fullWidth type='submit' disabled={buttonState} style={{ backgroundColor: 'black' }}>
                            {postData !== undefined ? 'Edituj post' : 'Kreiraj post'}
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Container>
    );
}

export default PostForm;

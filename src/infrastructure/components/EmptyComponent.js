import React from 'react';

import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

import Spacer from './Spacer';
import { LocalizationContext } from '../../context/LanguageContext';

const EmptyComponent = (text) => {
    const { t } = React.useContext(LocalizationContext);

    return (
        <>
            <Spacer height={'10rem'} />
            <Container component='main' maxWidth='md'>
                <Typography component='h1' variant='h2' sx={{textAlign: 'center'}}>
                    {text.text}
                </Typography>
            </Container>

        </>
    );
}

export default EmptyComponent;
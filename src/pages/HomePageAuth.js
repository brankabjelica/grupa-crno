import React from 'react';

import { Button, Link } from '@mui/material';

import { LocalizationContext } from '../context/LanguageContext';

const HomePageAuth = () => {
    const { t } = React.useContext(LocalizationContext);


    return (
        <>
            <div>
                {t('homePage')}
            </div>
            <Button color='primary' variant='contained' sx={{ backgroundColor: 'black' }}>
                <Link href='/login' color='inherit' underline='none'>
                    Login
                </Link>
            </Button>
            <Button color='primary' variant='contained' sx={{ backgroundColor: 'black' }}>
                <Link href='/register' color='inherit' underline='none'>
                    Register
                </Link>
            </Button>
        </>
    );
}

export default HomePageAuth;

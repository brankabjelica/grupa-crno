import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import * as yup from 'yup';
import { useFormik } from 'formik';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'

import { LocalizationContext } from '../../context/LanguageContext';
import Spacer from '../../infrastructure/components/Spacer'
import client_auth from '../../apis/client_auth';


const Login = () => {
    const { t } = React.useContext(LocalizationContext);
    const navigate = useNavigate()

    const [error, setError] = React.useState('');

    const [showPassword, setShowPassword] = useState(false);

    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleMouseDownPassword = () => setShowPassword(!showPassword);

    const initialValues = {
        username: localStorage.getItem('username') ?? '',
        password: localStorage.getItem('password') ?? '',
        rememberMe: false
    }


    const onSignIn = async(values) => {
        try {
            const loginData = {
                username: values.username,
                password: values.password
            }


            const response = await client_auth.post('/auth/jwt/create/', loginData );

            const tok = JSON.stringify(response.data);
            const parsedData = JSON.parse(tok);
    
            localStorage.setItem('token', parsedData.access);
            localStorage.setItem('refreshToken', parsedData.refresh);
    
    
            if (values.rememberMe === true) {
                localStorage.setItem('username', values.username);
                localStorage.setItem('password', values.password);
            }
    
    
            navigate('/');
            navigate(0);
    
        } catch (error) {
            setError('Greska')
            return

        }
    }

    const validationSchema = yup.object({
        username: yup.string()
            .required('Obavezno polje'),
        password: yup.string()
            .required('Obavezno polje')
    });

    const formik = useFormik({
        initialValues: initialValues,
        validationSchema: validationSchema,
        onSubmit: onSignIn,
    });


    return (
        <>
            <Spacer height={'10rem'} />
            <Container component='main' maxWidth='sm'>

                <form onSubmit={formik.handleSubmit} className='mt-3 m-2'>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id='username'
                                name='username'
                                variant='outlined'
                                label={'Korisnicko ime'}
                                value={formik.values.username}
                                onChange={formik.handleChange}
                                error={formik.touched.username && Boolean(formik.errors.username)}
                                helperText={formik.touched.username && formik.errors.username}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id='password'
                                label={'Lozinka'}
                                name='password'
                                variant='outlined'
                                type={showPassword ? 'text' : 'password'}
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={formik.touched.password && formik.errors.password}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position='end'>
                                            <IconButton
                                                aria-label='toggle password visibility'
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                                <FormControlLabel
                                    name='rememberMe'
                                    control={<Checkbox value={formik.values.rememberMe} onChange={formik.handleChange} />}
                                    label={'Zapamti me'}
                                />
                            </Grid>
                        {error !== '' && <Grid item xs={12}><p style={{ color: 'red' }}>{error}</p></Grid>}
                        <Grid item xs={12}>
                            <Button color='primary' variant='contained' fullWidth type='submit' style={{ backgroundColor: 'black' }}>
                                {'Uloguj se'}
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </>
    );
};

export default Login;

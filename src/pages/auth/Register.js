import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import * as yup from 'yup';
import { useFormik } from 'formik';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton'

import { LocalizationContext } from '../../context/LanguageContext';
import Spacer from '../../infrastructure/components/Spacer'
import client_auth from '../../apis/client_auth';

const initialValues = {
    name: '',
    username: '',
    email: '',
    password: '',
    re_password: '',
}


const Register = () => {
    const { t } = React.useContext(LocalizationContext);
    const navigate = useNavigate()

    const [error, setError] = React.useState('');

    const [showPassword, setShowPassword] = useState(false);
    const [showRePassword, setShowRePassword] = useState(false);

    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleMouseDownPassword = () => setShowPassword(!showPassword);

    const handleClickShowRePassword = () => setShowRePassword(!showRePassword);
    const handleMouseDownRePassword = () => setShowRePassword(!showRePassword);




    const onSignUp = async(values) => {
        try {
            const newUser = {
                name: values.name,
                username: values.username,
                email: values.email,
                password: values.password,
                re_password: values.re_password,
            };

            console.log(newUser)

            const reg = await client_auth.post(`/auth/users/`, newUser)

           if (reg.status === 201) {
            alert('Uspesno ste se registrovali')
            navigate('/login')
            navigate(0)
           }


        } catch (error) {
            setError('Greska')
            return

        }
    }

    const validationSchema = yup.object({
        name: yup.string()
            .required('Obavezno polje'),
        username: yup.string()
            .required('Obavezno polje'),
        email: yup.string()
            .email('Neispravan imejl')
            .required('Obavezno polje'),
        password: yup.string()
            .required('Obavezno polje')
            .min(8, 'Minimum 8 karaktera')
            .max(40, 'Maksimum 40 karaktera'),
        re_password: yup.string()
            .required('Obavezno polje')
            .oneOf([yup.ref('password'), null], 'Lozinke se ne podudaraju'),
    });

    const formik = useFormik({
        initialValues: initialValues,
        validationSchema: validationSchema,
        onSubmit: onSignUp,
    });


    return (
        <>
            <Spacer height={'10rem'} />
            <Container component='main' maxWidth='sm'>

                <form onSubmit={formik.handleSubmit} className='mt-3 m-2'>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id='name'
                                name='name'
                                variant='outlined'
                                label={'Ime i prezime'}
                                value={formik.values.name}
                                onChange={formik.handleChange}
                                error={formik.touched.name && Boolean(formik.errors.name)}
                                helperText={formik.touched.name && formik.errors.name}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id='username'
                                name='username'
                                variant='outlined'
                                label={'Korisnicko ime'}
                                value={formik.values.username}
                                onChange={formik.handleChange}
                                error={formik.touched.username && Boolean(formik.errors.username)}
                                helperText={formik.touched.username && formik.errors.username}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id='email'
                                name='email'
                                variant='outlined'
                                label={'Imejl'}
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={formik.touched.email && formik.errors.email}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id='password'
                                label={'Lozinka'}
                                name='password'
                                variant='outlined'
                                type={showPassword ? 'text' : 'password'}
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={formik.touched.password && formik.errors.password}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position='end'>
                                            <IconButton
                                                aria-label='toggle password visibility'
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id='re_password'
                                label={'Ponovljena lozinka'}
                                name='re_password'
                                variant='outlined'
                                type={showRePassword ? 'text' : 'password'}
                                value={formik.values.re_password}
                                onChange={formik.handleChange}
                                error={formik.touched.re_password && Boolean(formik.errors.re_password)}
                                helperText={formik.touched.re_password && formik.errors.re_password}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position='end'>
                                            <IconButton
                                                aria-label='toggle password visibility'
                                                onClick={handleClickShowRePassword}
                                                onMouseDown={handleMouseDownRePassword}
                                            >
                                                {showRePassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        {error !== '' && <Grid item xs={12}><p style={{ color: 'red' }}>{error}</p></Grid>}
                        <Grid item xs={12}>
                            <Button color='primary' variant='contained' fullWidth type='submit' style={{ backgroundColor: 'black' }}>
                                {'Registruj se'}
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </>
    );
};

export default Register;

import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';

import client from '../apis/client'
import { LocalizationContext } from '../context/LanguageContext';
import PostList from './post/PostList';
import PostForm from './forms/PostForm';
import Spacer from '../infrastructure/components/Spacer';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: '2px solid black',
  boxShadow: 24,
  p: 1
};


const PostPage = () => {
  const { t } = React.useContext(LocalizationContext);
  let navigate = useNavigate()

  const [show, setShow] = useState(false);
  const [posts, setPosts] = useState();
  const [profileId, setProfileId] = useState();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  useEffect(() => {
    let isActive = true;
    const loadData = async () => {

      const profile = await client.get(`/profiles/`);
      const post = await client.get(`/profiles/${await profile.data[0].id}/posts/`);

      if (isActive) {
        if (profile.data.length === 0) {
          navigate('/')
          navigate(0)

        }
        else {
          setPosts(await post.data)
          setProfileId(await profile.data[0].id)
        }
      }
    };

    if (isActive) loadData();

    return () => {
      isActive = false;
    };

  }, []);
  return (
    <>


      <Spacer height={'10rem'} />
      <Button onClick={handleShow} color='primary' variant='contained' style={{ backgroundColor: 'black' }}>
        {'Dodaj post'}
      </Button>
      {posts !== undefined &&
        <PostList posts={posts} profileId={profileId} />
      }
      <Modal open={show} onClose={handleClose} style={{ overflow: 'scroll' }}>
        <Box sx={style}>
          <PostForm profileId={profileId} />
        </Box>
      </Modal>
    </>
  );
};

export default PostPage;